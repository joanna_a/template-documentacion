# Template Documentación



## Para empezar

Este proyecto está desarrollado en React Native, que usa como lenguaje base Javascript.
Para compilar este proyecto móvil necesitamos un dispositivo físico o usar emuladores de android (windows) o apple (mac);
Si es la primera vez que compilas un proyecto de RN en tu equipo, puedes seguir esta guía de configuración de entorno.
https://reactnative.dev/docs/environment-setup

## Compilación

Una vez que hayas configurado el entorno, procede a clonar el repositorio en tu equipo. Ya que se termine de copiar, abre la terminal de tu preferencia y dentro del proyecto ejectuta:

```
npm install

```
Después de instalar las dependencias necesarias, puedes correr en android con el comando react-native run-android o react-native run-ios.

## ¿Qué puede salir mal en la compilación?

*Si no tienes instalado el SDK de Android en tu equipo windows, no podrás compilar el proyecto. (sigue los pasos de configuración de entorno)
*En algunas ocasiones el comando react-native run-ios no compila el proyecto, si esto pasa puedes compilar desde Xcode en un dispositivo fisico o instalar los emuladores de apple.

